<?php declare(strict_types=1);

namespace BasicAlgorithms\PHP\tests\unit;

use BasicAlgorithms\PHP\BinarySearch;
use PHPUnit\Framework\TestCase;

class BinarySearchTest extends TestCase
{
    public function testPositionCorrect(): void
    {
        $this->assertEquals(1, BinarySearch::run([1, 3, 5, 7, 9], 3), 'Unexpected position');
    }

    public function testPositionStringCorrect(): void
    {
        $this->assertEquals(2, BinarySearch::run(['eight', 'five', 'four', 'nine', 'one', 'seven', 'six', 'ten', 'three', 'two'], 'four'), 'Unexpected position');
    }

    public function testPositionNotFound(): void
    {
        $this->assertNull(BinarySearch::run([1, 3, 5, 7, 9], -1), 'Position is not null');
    }
}
