<?php declare(strict_types=1);

namespace BasicAlgorithms\PHP;

class BinarySearch
{
    /**
     * Метод, реализующий алгоритм двоичного поиска.
     * Возвращает позицию целого числа или строки со значением $needle в массиве $haystack.
     * Если такого значения в массиве нет, возвращает null.
     *
     * @param array $haystack Отсортированный массив целых чисел
     * @param int|string $needle Целочисленное или строковое значение искомого элемента
     * @return int|null Позиция искомого элемента в массиве
     */
    public static function run(array $haystack, $needle): ?int
    {
        $low = 0;
        $high = count($haystack);

        while ($low <= $high) {
            // Получаем среднюю позицию путём суммы минимального и максимального, и делением пополам
            $mid = (int)floor(($low + $high) / 2);
            // Выбираем из массива предположительное значение по средней позиции
            $guess = $haystack[$mid];

            if ($needle === $guess) {
                // Если предполагаемое значение совпало с искомым - возвращаем результат
                return $mid;
            }

            if ($guess > $needle) {
                // Если предполагаемое значение оказалось больше искомого - переопределим максимальное значение
                $high = $mid - 1;
            } else {
                // Иначе переопределим минимальное значение
                $low = $mid + 1;
            }
        }

        return null;
    }
}
